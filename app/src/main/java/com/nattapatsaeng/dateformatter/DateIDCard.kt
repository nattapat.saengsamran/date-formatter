package com.nattapatsaeng.dateformatter

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.Year
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder

enum class DateType {
    LocalDate, YearMonth, Year
}

@RequiresApi(Build.VERSION_CODES.O)
class DateIDCard(date: String) {
    private lateinit var type: DateType
    private lateinit var convertedDate: LocalDate

    init {
        val newDate = removeDashSpaceAndDot(date)
        checkDateType(newDate)
        checkFormat(newDate)
    }

    private fun checkDateType(word: String) {
        if (word.length == 4) {
            this.type = DateType.Year
        }
        else if (word.length == 7) {
            this.type = DateType.YearMonth
        }
        else if (word.length == 8 || word.length == 9) {
            this.type = DateType.LocalDate
        }
        else {
            throw Exception("DateIDCard: Length of string not match")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun checkFormat(word: String) {
        try {
            if (this.type == DateType.LocalDate) {
                val builder = DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("dMMMyyyy")
                val dateFormat = builder.toFormatter()

                convertedDate = LocalDate.parse(word, dateFormat)
            }
            else if (this.type == DateType.YearMonth) {
                val dateFormat = DateTimeFormatter.ofPattern("MMMyyyy")

                var dateYearMonth = YearMonth.parse(word, dateFormat)
                convertedDate = dateYearMonth.atDay(1)
            }
            else {
                val dateFormat = DateTimeFormatter.ofPattern("yyyy")

                var dateYear = Year.parse(word, dateFormat)
                var dateYearMonth = dateYear.atMonth(1)
                convertedDate = dateYearMonth.atDay(1)
            }

        }
        catch (e: Exception) {
            throw Exception("DateIDCard: Format is not supported. Only [yyyy], [MMMyyyy] or [dMMMyyyy]")
        }
    }

    private fun removeDashSpaceAndDot(word: String): String {
        val re = Regex("[^A-Za-z0-9 ]")
        return re.replace(word, "")
    }

    fun toLocalDate(): LocalDate {
        return convertedDate
    }

    fun getType(): DateType {
        return type
    }

    override fun toString(): String {
        if (this.type == DateType.LocalDate) {
            return convertedDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
        }
        else if (this.type == DateType.YearMonth) {
            return convertedDate.format(DateTimeFormatter.ofPattern("-/MM/yyyy"))
        }
        else {
            return convertedDate.format(DateTimeFormatter.ofPattern("-/-/yyyy"))
        }
    }


}