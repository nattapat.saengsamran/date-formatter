package com.nattapatsaeng.dateformatter

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.Year
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder

class DateValidator {
    @RequiresApi(Build.VERSION_CODES.O)
    fun isDateValid(word: String): Boolean {
        try {
            if (word.substring(0,4) == "-/-/") {
                val builder = DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("-/-/yyyy")
                val dateFormat = builder.toFormatter()

                Year.parse(word, dateFormat)
            }
            else if (word.substring(0,2) == "-/") {
                val builder = DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("-/MM/yyyy")
                val dateFormat = builder.toFormatter()

                YearMonth.parse(word, dateFormat)
            }
            else {
                val builder = DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("dd/MM/yyyy")
                val dateFormat = builder.toFormatter()

                LocalDate.parse(word, dateFormat)
            }

        }
        catch (e: Exception) {
            return false
        }

        return true
    }
}