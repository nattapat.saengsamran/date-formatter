package com.nattapatsaeng.dateformatter

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var date0 = DateIDCard("31JAN2012")
        Log.d("Test",date0.toString())

        if (DateValidator().isDateValid("-/-/2012")) {
            Log.d("Test", "Valid")
        }
        else {
            Log.d("Test", "Invalid")
        }
    }
}